<?php
/***************************************************************************************
* Warning !! DesjardinsConfig contains the key, you have to protect this file with all     *   
* the mechanism available in your development environment.                             *
* You may for instance put this file in another directory and/or change its name       *
***************************************************************************************/

define ("DESJARDINS_CLE", "12345678901234567890123456789012345678P0");
define ("DESJARDINS_TPE", "0000001");
define ("DESJARDINS_VERSION", "3.0");
define ("DESJARDINS_SERVEUR", "https://paiement.creditmutuel.fr/test/");
define ("DESJARDINS_CODESOCIETE", "yours");
define ("DESJARDINS_URLOK", "http://www.google.fr");
define ("DESJARDINS_URLKO", "http://www.google.nz");
?>
