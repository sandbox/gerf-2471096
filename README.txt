This module provides a gateway between the Monetico payment solution, also
called Cybermut, and Drupal Commerce module.
It works for Desjardins cooperative financial group.

Installation and configuration:

- Download module from drupal.org
- Uncompress it into your module directory
- Enable the module
- Go to Store -> Configuration -> Payment ... And edit the rules with your own
datas. You can choose if payment gateway need to be enable for test or
production website.
- Enable the rules if it doesn't.
